#ifndef MESHHELPER_H
#define MESHHELPER_H
#include "polymesh.h"

namespace MeshHelper
{
Vertices selected_vertices(PolyMesh *mesh);
Edges selected_edges(PolyMesh *mesh);
Faces selected_faces(PolyMesh *mesh);
void clear_vertex_selection(PolyMesh *mesh);
void clear_edge_selection(PolyMesh *mesh);
void clear_face_selection(PolyMesh *mesh);

int n_selected_vertices(PolyMesh *mesh);
void export_selected_vertices_information(PolyMesh *mesh);
void export_selected_faces_information(PolyMesh *mesh);
void export_selected_edges_information(PolyMesh *mesh);

Vertices one_ring_edge_vertices_without_center(PolyMesh *mesh, OpenMesh::VertexHandle v);
Vertices one_ring_edge_vertices(PolyMesh *mesh, OpenMesh::VertexHandle v);
Vertices k_ring_edge_vertices(PolyMesh *mesh, OpenMesh::VertexHandle v, int k);

// important for polygonal meshes (non-triangles)
Vertices one_ring_face_vertices(PolyMesh *mesh, OpenMesh::VertexHandle v);

std::vector<OpenMesh::Vec3d> points(PolyMesh *mesh, Vertices vs);
Vertices fvs(PolyMesh *mesh, OpenMesh::FaceHandle f);
std::vector<OpenMesh::Vec3d> fps(PolyMesh *mesh, OpenMesh::FaceHandle f);
OpenMesh::Vec3d vectorial_area(std::vector<OpenMesh::Vec3d> ps);
OpenMesh::Vec3d vectorial_area(PolyMesh *mesh, OpenMesh::FaceHandle f);
OpenMesh::Vec3d face_normal(PolyMesh *mesh, OpenMesh::FaceHandle f);

OpenMesh::Vec3d vertex_normal_one_ring(PolyMesh *mesh, OpenMesh::VertexHandle v);

void export_mesh_vertex_colors(PolyMesh *mesh);
void initialize_halfedge_normals(PolyMesh *mesh);

void set_feature_edges(PolyMesh *mesh, Edges es);
void set_feature_edges(PolyMesh *mesh, double degree);
Edges feature_edges(PolyMesh *mesh, double degree);
}

#endif // MESHHELPER_H
