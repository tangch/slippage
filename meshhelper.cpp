#include "meshhelper.h"
#include "slippagefeature.h"
#include <iostream>
#include <set>
#include <math.h>
using namespace std;
using namespace OpenMesh;

namespace{
ostream &operator <<(ostream &q, OpenMesh::Vec3d p){
    q << p[0] << " " << p[1] << " " << p[2];
    return q;
}

bool is_feature(PolyMesh *mesh, EdgeHandle e, double degree)
{
    if(mesh->is_boundary(e)) return false;
    // get the two halfedge handles
    HalfedgeHandle h0 = mesh->halfedge_handle(e, 0);
    HalfedgeHandle h1 = mesh->halfedge_handle(e, 1);
    FaceHandle f0 = mesh->face_handle(h0);
    FaceHandle f1 = mesh->face_handle(h1);
    // get the normal by vectorial area
    Vec3d fn0 = MeshHelper::face_normal(mesh, f0);
    Vec3d fn1 = MeshHelper::face_normal(mesh, f1);

    double radius = M_PI*degree/180.;
    if( (fn0|fn1)<=std::cos(radius) )
        return true;
    else
        return false;
}

}

Faces MeshHelper::selected_faces(PolyMesh *mesh)
{
    Faces fs;
    for(auto f : mesh->faces())
        if(mesh->status(f).selected())
            fs.push_back(f);
    return fs;
}


Edges MeshHelper::selected_edges(PolyMesh *mesh)
{
    Edges es;
    for(auto e : mesh->edges())
        if(mesh->status(e).selected())
            es.push_back(e);
    return es;
}

Vertices MeshHelper::selected_vertices(PolyMesh *mesh)
{
    Vertices vs;
    for(auto v : mesh->vertices())
        if(mesh->status(v).selected())
            vs.push_back(v);
    return vs;
}

void MeshHelper::clear_vertex_selection(PolyMesh *mesh)
{
    for(const auto &v : mesh->vertices())
        mesh->status(v).set_selected(false);
}

void MeshHelper::clear_edge_selection(PolyMesh *mesh)
{
    for(const auto &e : mesh->edges())
        mesh->status(e).set_selected(false);
}

void MeshHelper::clear_face_selection(PolyMesh *mesh)
{
    for(const auto &f : mesh->faces())
        mesh->status(f).set_selected(false);
}

int MeshHelper::n_selected_vertices(PolyMesh *mesh)
{
    return selected_vertices(mesh).size();
}

void MeshHelper::export_selected_faces_information(PolyMesh *mesh)
{
    for(auto f : selected_faces(mesh))
        cout << "Selected Face: " << f.idx();
}

void MeshHelper::export_selected_edges_information(PolyMesh *mesh)
{
    for(auto e : selected_edges(mesh))
        cout << "Selected Edge: " << e.idx();
}

void MeshHelper::export_selected_vertices_information(PolyMesh *mesh)
{
    for(auto v : selected_vertices(mesh))
        cout << "Selected Vertex " << v.idx() << ": " << mesh->point(v);
}

Vertices MeshHelper::one_ring_edge_vertices(PolyMesh *mesh, OpenMesh::VertexHandle v)
{
    Vertices vs{v};
    for(auto vv : mesh->vv_range(v))
        vs.push_back(vv);
    return vs;
}

Vertices MeshHelper::one_ring_edge_vertices_without_center(PolyMesh *mesh, OpenMesh::VertexHandle v)
{
    Vertices vs;
    for(auto vv : mesh->vv_range(v))
        vs.push_back(vv);
    return vs;
}

Vertices MeshHelper::one_ring_face_vertices(PolyMesh *mesh, OpenMesh::VertexHandle v)
{
    set<VertexHandle> v_set{v};
    for(auto f : mesh->vf_range(v))
        for(auto fv : mesh->fv_range(f))
            v_set.insert(fv);
    Vertices vs(v_set.begin(), v_set.end());
    return vs;
}

Vec3d MeshHelper::vectorial_area(std::vector<Vec3d> ps)
{
    Vec3d a(0,0,0);
    for(int i=0, N=ps.size(); i<N; ++i)
        a += .5*ps[i]%ps[(i+1)%N];
    return a;
}

vector<VertexHandle> MeshHelper::fvs(PolyMesh *mesh, FaceHandle f)
{
    vector<VertexHandle> vs;
    for(auto v: mesh->fv_range(f)) vs.push_back(v);
    return vs;
}

vector<Vec3d> MeshHelper::fps(PolyMesh *mesh, FaceHandle f)
{
    return points(mesh, fvs(mesh, f));
}

std::vector<Vec3d> MeshHelper::points(PolyMesh *mesh, Vertices vs)
{
    std::vector<Vec3d> ps;
    for(const auto &v : vs)
        ps.push_back(mesh->point(v));
    return ps;
}

Vec3d MeshHelper::face_normal(PolyMesh *mesh, FaceHandle f)
{
    return vectorial_area(fps(mesh, f)).normalize();
}

Vec3d MeshHelper::vectorial_area(PolyMesh *mesh, FaceHandle f)
{
    return vectorial_area(fps(mesh, f));
}

Vec3d MeshHelper::vertex_normal_one_ring(PolyMesh *mesh, VertexHandle v)
{
    return vectorial_area(points(mesh, one_ring_edge_vertices_without_center(mesh, v)));
}

void MeshHelper::export_mesh_vertex_colors(PolyMesh *mesh)
{
    for(auto v : mesh->vertices())
    {
        cout << "Color of Vertex " << v.idx() << mesh->color(v)[0] << mesh->color(v)[1] << mesh->color(v)[2];
    }
}

void MeshHelper::initialize_halfedge_normals(PolyMesh *mesh)
{
    mesh->request_halfedge_normals();
    for(const auto &h : mesh->halfedges())
    {
    }
}

// TODO or Not TODO?
Vertices MeshHelper::k_ring_edge_vertices(PolyMesh *mesh, VertexHandle v, int k)
{

}

void MeshHelper::set_feature_edges(PolyMesh *mesh, Edges es)
{
    for(const auto &e : mesh->edges())
        mesh->status(e).set_feature(false);
    for(const auto &e : es)
        mesh->status(e).set_feature(true);
}

void MeshHelper::set_feature_edges(PolyMesh *mesh, double degree)
{
    set_feature_edges(mesh, feature_edges(mesh, degree));
}

Edges MeshHelper::feature_edges(PolyMesh *mesh, double degree)
{
    Edges es;
    for(const auto &e : mesh->edges())
        if(is_feature(mesh, e, degree))
            es.push_back(e);
    return es;
}




























