#ifndef POLYMESH_H
#define POLYMESH_H

#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>
#include <OpenMesh/Core/Mesh/PolyMeshT.hh>
#include <Eigen/Eigen>
#include <Eigen/Sparse>
#include <set>
#include <memory>
typedef std::vector<OpenMesh::VertexHandle> Vertices;
typedef std::vector<OpenMesh::FaceHandle> Faces;
typedef std::vector<OpenMesh::EdgeHandle> Edges;

struct MyTraits : public OpenMesh::DefaultTraits
{
    typedef OpenMesh::Vec3d Point;
    typedef OpenMesh::Vec3d Normal;
    typedef OpenMesh::Vec3d Color;

    VertexAttributes( OpenMesh::Attributes::Normal |
        OpenMesh::Attributes::Color |
        OpenMesh::Attributes::Status);

    FaceAttributes( OpenMesh::Attributes::Normal |
        OpenMesh::Attributes::Color |
        OpenMesh::Attributes::Status);

    EdgeAttributes( OpenMesh::Attributes::Normal |
        OpenMesh::Attributes::Color |
        OpenMesh::Attributes::Status);

    VertexTraits
    {
    public:
        bool corner;
        bool linear_dependency; // can be represented as linear combination of other vertices
        std::vector<std::pair<OpenMesh::VertexHandle, double> > linear_relation;  // dependent vertices and the coefficient
        VertexT() : corner(false), linear_dependency(false) {}

        std::vector<double> slippage_eigen_values() const
        {
            return slippage_eigen_values_;
        }

        void set_slippage_eigen_values(const std::vector<double> &slippage_eigen_values)
        {
            slippage_eigen_values_ = slippage_eigen_values;
        }

        private:
        std::vector<double> slippage_eigen_values_;
    };

    FaceTraits
    {
        public:
        bool planar;
        FaceT() : planar(true) {}
    };
};

typedef OpenMesh::PolyMesh_ArrayKernelT<MyTraits> PolyMesh;
typedef std::shared_ptr<PolyMesh> MeshP;
#endif // POLYMESH_H
