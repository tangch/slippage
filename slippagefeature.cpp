#include "slippagefeature.h"
#include <Eigen/Core>
#include <OpenMesh/Core/Geometry/VectorT.hh>
#include <iostream>
#include "meshhelper.h"
using namespace OpenMesh;
using namespace Eigen;
using namespace std;

typedef Matrix<double, 6, 6> Matrix6d;
typedef Matrix<double, 6, 1> Vector6d;
typedef pair<Vec3d, Vec3d> Vec3Pair;
namespace{
double default_slippage_cutoff = 0.001;
SlippageFeature::Type type = SlippageFeature::Type::OneRingVertexNormal;

Matrix6d covariance_matrix(Vec3d p, Vec3d n)
{
    Matrix6d mat;
    VectorXd vec(6);
    Vec3d c = p%n;
    vec << c[0], c[1], c[2], n[0], n[1], n[2];
    mat = vec * vec.transpose();
    return mat;
}

Matrix6d covariance_matrix(vector<Vec3Pair> pns)
{
    cout << "Number of Pairs: " << pns.size();
    Matrix6d mat;
    mat.setZero();
    for(auto pn : pns)
        mat += covariance_matrix(pn.first, pn.second);
    return mat;
}

vector<double> eigen_values(Matrix6d mat)
{
    EigenSolver<Matrix6d> solver(mat);
    vector<double> evals;
    auto ev = solver.eigenvalues().real();
    std::cout << mat << endl;
    for(int i=0; i<ev.size(); ++i)
        evals.push_back(ev[i]);
    return evals;
}

int extract_feature(vector<double> evals, double cutoff_ratio)
{
    if(evals.size()==0)
    {
        cout << "No Eigen Values Stored!";
        return 0;
    }
    double max = *std::max_element(evals.begin(), evals.end());
    return count_if(evals.begin(), evals.end(), [&max, &cutoff_ratio](double d){
        return d < max*cutoff_ratio; });
}

vector<double> slippage_eigen_values_by_vertex_normals(PolyMesh *mesh, Vertices vs)
{
    vector<Vec3Pair> pns;
    for(auto v : vs)
        pns.push_back(make_pair(mesh->point(v), mesh->normal(v)));
    Matrix6d mat = covariance_matrix(pns);
//    cout << mat;
    return eigen_values(mat);
}

int slippage_by_vv_range(PolyMesh *mesh, VertexHandle v, double cutoff_ratio)
{
    Vertices vs{v};
    for(auto vv : mesh->vv_range(v))
        vs.push_back(vv);
    vector<double> evals = slippage_eigen_values_by_vertex_normals(mesh, vs);
    mesh->data(v).set_slippage_eigen_values(evals);
    return extract_feature(evals, cutoff_ratio);
}

int slippage_by_one_ring_face_vertices(PolyMesh *mesh, VertexHandle v, double cutoff_ratio)
{
    Vertices vs = MeshHelper::one_ring_face_vertices(mesh, v);
    vector<double> evals = slippage_eigen_values_by_vertex_normals(mesh, vs);
    return extract_feature(evals, cutoff_ratio);
}

// every vertex is put towards the system together with the edge normal, evaluated by the two adjacent faces
int slippage_by_adjacent_face_normals(PolyMesh *mesh, VertexHandle v, double cutoff_ratio)
{
    vector<Vec3Pair> pns;
    Vec3d p = mesh->point(v);
    Vec3d n = mesh->normal(v);
    pns.push_back(make_pair(p, n));
    for(auto h : mesh->voh_range(v))
    {
        auto vv = mesh->to_vertex_handle(h);
        p = mesh->point(vv);
        FaceHandle f0 = mesh->face_handle(h);
        FaceHandle f1 = mesh->face_handle(mesh->opposite_halfedge_handle(h));
        n = Vec3d(0, 0, 0);
        if(f0.idx()>=0) n += MeshHelper::vectorial_area(mesh, f0);
        if(f1.idx()>=0) n += MeshHelper::vectorial_area(mesh, f1);
        n.normalize();
        pns.push_back(make_pair(p, n));
        HalfedgeHandle next = mesh->next_halfedge_handle(h);
        auto vo = mesh->to_vertex_handle(next);
        if(vo!=v && f0.idx()>=0) // if this is not a triangle nor boundary
        {
            p = mesh->point(v);
            n = mesh->normal(f0);
            pns.push_back(make_pair(p,n ));
        }
    }
    Matrix6d mat = covariance_matrix(pns);
    vector<double> evals = eigen_values(mat);
    mesh->data(v).set_slippage_eigen_values(evals);
    return extract_feature(evals, cutoff_ratio);
}

} //Unnamed namespace

// add different choices for visualization schemes
int SlippageFeature::slippage(PolyMesh *mesh, VertexHandle v, double cutoff_ratio)
{
    switch(type)
    {
    case Type::OneRingVertexNormal:
        return slippage_by_vv_range(mesh, v, cutoff_ratio);
    case Type::OneRingFaceNormal:
        return slippage_by_adjacent_face_normals(mesh, v, cutoff_ratio);
    default:
        cout << "This type of slippage feature is not implemented here yet.";
        return -1;
    }
}

vector<int> SlippageFeature::slippage(PolyMesh *mesh, double cutoff_ratio)
{
    mesh->request_face_normals();
    for(auto f : mesh->faces())
    {
        Vec3d normal = MeshHelper::face_normal(mesh, f);
        mesh->set_normal(f, normal);
    }
    mesh->request_vertex_normals();
    for(auto v : mesh->vertices())
    {
        Vec3d n;
        mesh->calc_vertex_normal_correct(v, n);
        mesh->set_normal(v, n);;
    }
    vector<int> results;
    results.reserve(mesh->n_vertices());
    for(auto v : mesh->vertices())
        results.push_back(slippage(mesh, v, cutoff_ratio));
    return results;
}

void SlippageFeature::set_type(SlippageFeature::Type _n)
{
    type = _n;
}

std::vector<int> SlippageFeature::slippage_fast(PolyMesh *mesh, double cutoff_ratio)
{
    std::vector<int> features;
    features.reserve(mesh->n_vertices());
    for(const auto &v : mesh->vertices())
    {
        vector<double> evals = mesh->data(v).slippage_eigen_values();
        features.push_back(extract_feature(evals, cutoff_ratio));
    }
    return features;
}
