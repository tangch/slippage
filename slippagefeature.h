#ifndef SLIPPAGEFEATURE_H
#define SLIPPAGEFEATURE_H
#include "polymesh.h"

namespace SlippageFeature
{
    enum class Type{
        OneRingVertexNormal,
        TwoRingVertexNormal,
        ThreeRingVertexNormal,
        FourRingVertexNormal,
        FiveRingVertexNormal,
        OneRingFaceNormal
    };

    std::vector<int> slippage(PolyMesh *mesh, double cutoff_ratio);

    // use precomputed eigen values
    std::vector<int> slippage_fast(PolyMesh *mesh, double cutoff_ratio);

    int slippage(PolyMesh *mesh, OpenMesh::VertexHandle v, double cutoff_ratio);

    void set_type(Type _n);

}

#endif // SLIPPAGEFEATURE_H
